package com.example.haianh.demo.task;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;
import static java.lang.Math.*;
import com.example.haianh.demo.activity.LocationActivity;
import com.google.android.gms.maps.model.LatLng;
import java.io.IOException;
import java.util.List;

/**
 * Created by HaiAnh on 07/08/2017.
 */

public class Scanning extends AsyncTask<Void,Void,String[]> {

    private final double DISTANCE =5.0;

    private LocationActivity locationActivity;
    private LatLng curLocation;
    private ProgressDialog progressDialog;

    public Scanning(LocationActivity locationActivity, LatLng curLocation,int bearing, ProgressDialog progressDialog) {
        this.locationActivity = locationActivity;
        this.curLocation = curLocation;
        this.progressDialog = progressDialog;
        this.mbearing = bearing;
    }

    private int mbearing;

    private String[] routs = new String[16];
    private LatLng[] latLngs = new LatLng[16];

    @Override
    protected String[] doInBackground(Void... v) {

        List<Address> addressList;
        Geocoder geoCoders = new Geocoder(locationActivity);

        for(int i = 0; i <8; i++){
           latLngs[i] = nextPositions(curLocation,DISTANCE*(i+1),mbearing);
           latLngs[15-i] = nextPositions(curLocation,DISTANCE*(8-i),180+mbearing);
        }


        for (int i = 0; i < 16; i++) {
            try {
                addressList = geoCoders.getFromLocation(latLngs[i].latitude, latLngs[i].longitude, 1);
                routs[i] = addressList.get(0).getThoroughfare();
                Log.e("",routs[i]);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return routs;
    }



    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(locationActivity);
        progressDialog.setMessage(" Scanning ");
        progressDialog.setProgressStyle(DialogInterface.BUTTON_NEUTRAL);
        progressDialog.show();
    }

    @Override
    protected void onPostExecute(String[] strings) {
        super.onPostExecute(strings);
        progressDialog.cancel();

        locationActivity.ibRouts(routs);
    }


    private String xuLyTenDuong(String str) {
        int i = str.indexOf(" ");
        return str.substring(i + 1);
    }

    private LatLng nextPositions(LatLng latLng, double distanceInMetres, int bearing) {
        double bearingRad = toRadians(bearing);
        double latRad = toRadians(latLng.latitude);
        double lonRad = toRadians(latLng.longitude);
        int earthRadiusInMetres = 6371000;
        double distFrac = distanceInMetres / earthRadiusInMetres;

        double latitudeResult = asin(sin(latRad) * cos(distFrac) + cos(latRad) * sin(distFrac) * cos(bearingRad));
        double a = atan2(sin(bearingRad) * sin(distFrac) * cos(latRad), cos(distFrac) - sin(latRad) * sin(latitudeResult));
        double longitudeResult = (lonRad + a + 3 * PI) % (2 * PI) - PI;

        return new LatLng(toDegrees(latitudeResult),toDegrees(longitudeResult));
    }
}

