package com.example.haianh.demo.task;

import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;
import com.example.haianh.demo.activity.LocationActivity;
import java.util.List;


public  class GetAddressTask extends AsyncTask<String, Void, String> {

    private LocationActivity locationActivity;


    public GetAddressTask(LocationActivity locationActivity) {
        this.locationActivity = locationActivity;
    }

    @Override
        protected String doInBackground(String... strings) {

            List<Address> addressList;
            Geocoder geoCoders = new Geocoder(locationActivity);
            try {
                addressList = geoCoders.getFromLocation(Double.parseDouble(strings[0]), Double.parseDouble(strings[1]), 1);
                String addressLine = addressList.get(0).getAddressLine(0);      //Đường
                String subAdminArea = addressList.get(0).getSubAdminArea();     //Thành phố
                String locality = addressList.get(0).getLocality();             //Quận
                String countryName = addressList.get(0).getCountryName();       //Quốc gia
                String subLocality = addressList.get(0).getSubLocality();       //Phường
                return "Số " + addressLine ;

            } catch (Exception ex) {
                return String.valueOf(Log.i("Lỗi đây nầy: ", ex.toString()));
            }
        }

        @Override
        protected void onPostExecute(String address) {
              super.onPostExecute(address);
              locationActivity.mLocationStr = address;
              locationActivity.locationResult();
        }
    }

