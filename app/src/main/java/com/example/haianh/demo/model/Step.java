package com.example.haianh.demo.model;


public class Step {
     public Start_location getStart_location() {
          return start_location;
     }

     public void setStart_location(Start_location start_location) {
          this.start_location = start_location;
     }

     private Start_location start_location;
     private End_location end_location;
     private String html_instructions;


     private Distance distance;

     public End_location getEnd_location() {
          return end_location;
     }

     public void setEnd_location(End_location end_location) {
          this.end_location = end_location;
     }

     public String getHtml_instructions() {
          return html_instructions;
     }

     public void setHtml_instructions(String html_instructions) {
          this.html_instructions = html_instructions;
     }

     public Distance getDistance() {
          return distance;
     }

     public void setDistance(Distance distance) {
          this.distance = distance;
     }
}



