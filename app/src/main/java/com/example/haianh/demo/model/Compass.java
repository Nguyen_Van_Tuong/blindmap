
package com.example.haianh.demo.model;

import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class Compass implements SensorEventListener {
    SensorManager mSensorManager;
    private float[] mLastAccelerometers;
    private float[] mLastMagnetometers;
    private boolean mLastAccelerometerSet;
    private boolean mLastMagnetometerSet;
    private float[] mOrientation = new float[3];
    private int mDirection;
    private Sensor mAccelerometer;
    private Sensor mMagnetometer;
    private OnDeviceRotationListener mOnDeviceRotationListener;

    public Compass(SensorManager sensorManager) {
        mSensorManager = sensorManager;
    }

    @Override
    public void onSensorChanged(android.hardware.SensorEvent sensorEvent) {
        switch (sensorEvent.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                mLastAccelerometers = sensorEvent.values.clone();
                mLastAccelerometerSet = true;
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                mLastMagnetometers = sensorEvent.values.clone();
                mLastMagnetometerSet = true;
                break;
            default:
                break;
        }
        float[] rotationMatrix = new float[9];
        if (mLastMagnetometerSet && mLastAccelerometerSet) {
            boolean success =
                    SensorManager.getRotationMatrix(rotationMatrix, null, mLastAccelerometers,
                            mLastMagnetometers);
            if (success) {
                SensorManager.getOrientation(rotationMatrix, mOrientation);
                int bearing = (int) Math.toDegrees(mOrientation[0]);
                mDirection = (bearing+360)%360;
                if (mOnDeviceRotationListener != null) {
                    mOnDeviceRotationListener.onRotate(mDirection);
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public int getDirection() {
        return mDirection;
    }

    public void registerSensor() {
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_UI);
    }

    public void unregisterSensor() {
        mSensorManager.unregisterListener(this, mAccelerometer);
        mSensorManager.unregisterListener(this, mMagnetometer);
    }

    public void setOnDeviceRotationListener(OnDeviceRotationListener onDeviceRotationListener) {
        mOnDeviceRotationListener = onDeviceRotationListener;
    }
}