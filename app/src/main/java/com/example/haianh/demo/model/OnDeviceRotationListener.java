package com.example.haianh.demo.model;


public interface OnDeviceRotationListener {
    void onRotate(int bearing);
}
